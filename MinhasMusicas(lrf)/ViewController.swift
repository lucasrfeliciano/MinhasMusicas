//
//  ViewController.swift
//  MinhasMusicas(lrf)
//
//  Created by COTEMIG on 18/08/22.
//

import UIKit

struct Musica {
    let nomeMusica: String
    let nomeAlbum: String
    let NomeCantor: String
    let NomeImagemPequena: String
    let NomeImagemGrande: String
}

class ViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    var listaDeMusicas:[Musica] = []
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.listaDeMusicas.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "MinhaCelula", for:indexPath) as!MyCell
        let musica = self.listaDeMusicas[indexPath.row]
        
        cell.musica.text = musica.nomeMusica
        cell.album.text = musica.nomeAlbum
        cell.cantor.text = musica.NomeCantor
        cell.capa.image = UIImage(named: musica.NomeImagemPequena)
        
        return cell
    }
    func tableView( _ tableView:UITableView,didSelectRowAt indexPath: IndexPath){
        self.performSegue(withIdentifier: "abrirDetalhe", sender: indexPath.row)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        let detalhesViewController = segue.destination as! DetalheMusicaViewController
        let indice = sender as! Int
        let musica = self.listaDeMusicas[indice]
        
        detalhesViewController.nomeImagem = musica.NomeImagemGrande
        detalhesViewController.nomeMusica = musica.nomeMusica
        detalhesViewController.nomeAlbum = musica.nomeAlbum
        detalhesViewController.nomeCantor = musica.NomeCantor
        
    }

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.dataSource = self
        self.tableView.delegate = self
        
        
        self.listaDeMusicas.append(Musica(nomeMusica: "Pontos Cardeais",nomeAlbum: "Álbum Vivo!",NomeCantor: "Alceu Valença",NomeImagemPequena: "capa_alceu_pequeno",NomeImagemGrande: "capa_alceu_grande" ))
        self.listaDeMusicas.append(Musica(nomeMusica: "Menor Abandonado",nomeAlbum: "Álbum Patota de Cosme",NomeCantor: "Zeca Pagodinho",NomeImagemPequena: "capa_zeca_pequeno",NomeImagemGrande: "capa_zeca_grande" ))
        self.listaDeMusicas.append(Musica(nomeMusica: "Tiro ao Álvaro",nomeAlbum: "Álbum Adoniran Barbosa e Convidados",NomeCantor: "Adoniran Barbosa",NomeImagemPequena: "capa_adoniran_pequeno",NomeImagemGrande: "capa_adoniran_grande" ))
        
        // Do any additional setup after loading the view.
    }


}

